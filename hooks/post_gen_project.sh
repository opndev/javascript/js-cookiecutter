#!/bin/sh

set -e

git init

git config commit.template .git-commit-template

git add -A
git commit --no-verify \
    -m "Initial commit of js-{{cookiecutter.repo_name}}"

remote=opndev-js-{{cookiecutter.repo_name}}

git remote add upstream git@gitlab.com:opndev/$remote
git remote add origin git@gitlab.com:{{cookiecutter.gitlab_username}}/$remote

exit 0
